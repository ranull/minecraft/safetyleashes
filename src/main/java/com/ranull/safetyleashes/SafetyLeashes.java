package com.ranull.safetyleashes;

import com.ranull.safetyleashes.command.SafetyLeashesCommand;
import com.ranull.safetyleashes.listener.EntityDamageByEntityListener;
import com.ranull.safetyleashes.listener.EntityDamageListener;
import org.bstats.bukkit.Metrics;
import org.bukkit.command.PluginCommand;
import org.bukkit.event.HandlerList;
import org.bukkit.plugin.java.JavaPlugin;

public class SafetyLeashes extends JavaPlugin {
    @Override
    public void onEnable() {
        saveDefaultConfig();
        registerMetrics();
        registerCommands();
        registerListeners();
    }

    @Override
    public void onDisable() {
        unregisterListeners();
    }

    private void registerMetrics() {
        new Metrics(this, 17363);
    }

    private void registerCommands() {
        PluginCommand unbreakableLeashesPluginCommand = getCommand("safetyleashes");

        if (unbreakableLeashesPluginCommand != null) {
            SafetyLeashesCommand unbreakableLeashesCommand = new SafetyLeashesCommand(this);

            unbreakableLeashesPluginCommand.setExecutor(unbreakableLeashesCommand);
            unbreakableLeashesPluginCommand.setTabCompleter(unbreakableLeashesCommand);
        }
    }

    private void registerListeners() {
        getServer().getPluginManager().registerEvents(new EntityDamageListener(this), this);
        getServer().getPluginManager().registerEvents(new EntityDamageByEntityListener(this), this);
    }

    private void unregisterListeners() {
        HandlerList.unregisterAll(this);
    }
}
