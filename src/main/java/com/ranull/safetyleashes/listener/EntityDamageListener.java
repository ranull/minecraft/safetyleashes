package com.ranull.safetyleashes.listener;

import com.ranull.safetyleashes.SafetyLeashes;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;

import java.util.List;

public class EntityDamageListener implements Listener {
    private final SafetyLeashes plugin;

    public EntityDamageListener(SafetyLeashes plugin) {
        this.plugin = plugin;
    }

    @EventHandler(priority = EventPriority.LOW, ignoreCancelled = true)
    public void onEntityDamage(EntityDamageEvent event) {
        if (!(event instanceof EntityDamageByEntityEvent) && event.getEntity() instanceof LivingEntity) {
            LivingEntity livingEntity = (LivingEntity) event.getEntity();
            List<String> protectHolderList = plugin.getConfig().getStringList("protect.holder");
            List<String> protectCauseList = plugin.getConfig().getStringList("protect.cause");

            if (livingEntity.isLeashed() && (!(livingEntity.getLeashHolder() instanceof Player)
                    || livingEntity.getLeashHolder().hasPermission("safetyleashes.use"))
                    && (protectHolderList.contains("ALL")
                    || protectHolderList.contains(livingEntity.getLeashHolder().getType().toString()))
                    && (protectCauseList.contains("ALL")
                    || protectCauseList.contains(event.getCause().toString()))) {
                event.setCancelled(true);
            }
        }
    }
}
