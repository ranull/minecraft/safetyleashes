package com.ranull.safetyleashes.listener;

import com.ranull.safetyleashes.SafetyLeashes;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

import java.util.List;

public class EntityDamageByEntityListener implements Listener {
    private final SafetyLeashes plugin;

    public EntityDamageByEntityListener(SafetyLeashes plugin) {
        this.plugin = plugin;
    }

    @EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true)
    public void onEntityDamageByEntity(EntityDamageByEntityEvent event) {
        if (event.getEntity() instanceof LivingEntity) {
            LivingEntity livingEntity = (LivingEntity) event.getEntity();
            List<String> protectHolderList = plugin.getConfig().getStringList("protect.holder");
            List<String> protectCauseList = plugin.getConfig().getStringList("protect.cause");
            List<String> damageDamagerList = plugin.getConfig().getStringList("damage.damager");

            if (livingEntity.isLeashed() && (!(livingEntity.getLeashHolder() instanceof Player)
                    || livingEntity.getLeashHolder().hasPermission("safetyleashes.use"))
                    && (protectHolderList.contains("ALL")
                    || protectHolderList.contains(livingEntity.getLeashHolder().getType().toString()))
                    && ((protectCauseList.contains("ALL") || protectCauseList.contains(event.getCause().toString())))
                    && (!damageDamagerList.contains("ALL")
                    && !damageDamagerList.contains(event.getDamager().getType().toString()))) {
                event.setCancelled(true);
            }
        }
    }
}
